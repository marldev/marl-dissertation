MARL Dissertation Template
--------------------------

This template should be the one used to submit the Ph.D Dissertation at MARL (NYU).

The LaTeX source files can be found under the ```src``` folder.

Authors
-------

Created by Taemin Cho.

Modified by Eric Humphrey and Oriol Nieto.
